class GenerateurSite extends Program{

    final int IDX_NOM = 0;
    final int IDX_DATE = 1;
    final int IDX_ENTREPRISE = 2;
    final int IDX_PRIX = 3;
    final int IDX_DESCRIPTION= 4;

    String[] rechercheValeur(String texte){
        String[] retourValeur=new String[5];
        int cpt=0;
        boolean condition = false;
        String chaine="";
        for (int i=1; i<length(texte);i=i+1){
            if (charAt(texte,i-1)==':' && charAt(texte,i)==' '){  
                condition=true;
            }else if (condition){
                if (charAt(texte,i)=='\n') {
                    retourValeur[cpt]= chaine;
                    chaine="";
                    cpt=cpt+1;
                    condition=false;
                } else {
                    chaine=chaine+charAt(texte,i);
                }
            }
        }
        return retourValeur;
    }
    
    String[][] chargerProduits(String repertoire, String prefixe) {
        String[][] retourValeur=new String[30][5];
        //String[] tab=new String[5];
        boolean condition = false;
        for (int idx=1;idx<=30;idx++){
            println(repertoire+prefixe+idx+".txt");
        }
            /*String[] stringTab = rechercheValeur(fileAsString(repertoire+prefixe+idx+".txt"));
            int idxC=0;
            condition = false;
            while (condition){
                retourValeur[idx-1][idxC]=stringTab[idxC];
                idxC=idxC+1;
                if (idxC==4){
                    condition=true;
                }
            }
        }*/
        return retourValeur;
    }

    /*String stringTab (String fileDirectory,String prefixe,int numProduit,int idxL){
        idxL=idxL-1;
        String retourIDX="";
        String[][] tab = chargerProduits(fileDirectory,prefixe);
        retourIDX=tab[idxL][numProduit];
        return retourIDX;
    }*/

    String genererSection(String fileDirectory,String prefixe,int idx){ // à changer

        String retourSection="";
        if (fileDirectory == "index"){
            retourSection="<h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>\n";
            retourSection+="        <p>\n";
            retourSection+="Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique.\n";
            retourSection+="        </p>\n";
        }else{
            String[][] tabClee = chargerProduits(fileDirectory, prefixe);
            retourSection+="        <h2>"+ tabClee[idx][IDX_NOM] + " (" + tabClee[idx][IDX_ENTREPRISE] + ")</h2>\n";
            retourSection+="        <h3>"+ tabClee[idx][IDX_PRIX] + " (Sortie en " + tabClee[idx][IDX_DATE] + ")</h3>\n";
            retourSection+="        <p>\n";
            retourSection+=tabClee[idx][IDX_DESCRIPTION] + "\n";
            retourSection+="        </p>";
        }
        return retourSection;
    }


    String genererProduit(String fileDirectory,String prefixe,int idx){  // à changer
        String retourProduit = "";
        int cpt=idx-1;
        int cpt1=idx;      
        int cpt2=1+idx;
        int cpt3=2+idx;
        int cpt4=3+idx;
        int cpt5=4+idx;
        String[] tabNomProduit = getAllFilesFromDirectory(fileDirectory);
        //for (int idxTab=0;idxTab<length(tabNomProduit);idxTab++){
        //    if (charAt(tabNomProduit[idxTab],7)<charAt(tabNomProduit[idxTab-1],7)){}
        //}
        retourProduit ="<!DOCTYPE html>"+"\n";
        retourProduit +="<html lang=\"fr\">"+"\n";
        retourProduit +="  <head>"+"\n";
        retourProduit +="    <title>Ordinateurs mythiques</title>"+"\n";
        retourProduit +="    <meta charset=\"utf-8\">"+"\n";
        retourProduit +="    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">"+"\n";
        retourProduit +="  </head>"+"\n";
        retourProduit +="  <body>"+"\n";
        retourProduit +="    <header>"+"\n";
        retourProduit +="      <h1>Ordinateurs mythiques</h1>"+"\n";
        retourProduit +="    </header>"+"\n";
        retourProduit +="    <nav>"+"\n";
        retourProduit +="      <ul>"+"\n";
        retourProduit +="        <li><a href=\"index.html\">Accueil</a></li>"+"\n";
        //for (int idx1=0;idx1<5;idx1++){
        //    retourProduit +="        <li><a href=\"produit"+cpt1+".html\">"+tabNomProduit[0]+"</a></li>"+"\n";
        //}
        retourProduit +="        <li><a href=\"produit"+cpt1+".html\">"+tabNomProduit[0]+"</a></li>"+"\n";
        retourProduit +="        <li><a href=\"produit"+cpt2+".html\">"+tabNomProduit[1]+"</a></li>"+"\n";
        retourProduit +="        <li><a href=\"produit"+cpt3+".html\">"+tabNomProduit[2]+"</a></li>"+"\n";
        retourProduit +="        <li><a href=\"produit"+cpt4+".html\">"+tabNomProduit[3]+"</a></li>"+"\n";
        retourProduit +="        <li><a href=\"produit"+cpt5+".html\">"+tabNomProduit[4]+"</a></li>"+"\n";
        retourProduit +="      </ul>"+"\n";
        retourProduit +="    </nav>"+"\n";
        retourProduit +="    <main>"+"\n";
        retourProduit +="      <section>"+"\n";
        retourProduit +=           genererSection(fileDirectory,prefixe,idx)+"\n";
        retourProduit +="      </section>"+"\n";
        retourProduit +="    </main>"+"\n";
        retourProduit +="  </body>"+"\n";
        retourProduit +="</html>";   
    return retourProduit;
    }


    void algorithm(){
        for (int i=1;i<=2;i++){// à changer
            print(genererProduit("data","/produit",i));

        /* 
        stringAsFile("output/index.html", genererProduit("index","produit",1));
        for (int i=1;i<=30;i++){// à changer
            stringAsFile("output/produit" + i + ".html", genererProduit("data/","produit",i)); 
        }
        /*String nomFichier = argument(0);
        if (fileExist(nomFichier)){
            String texte = fileAsString(nomFichier);
            println(genererProduit(texte)); 
        }else{
            error("le fichier n'existe pas");*/
        }
    }
}
