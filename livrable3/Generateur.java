class Generateur extends Program{

    final int IDX_NOM = 0;
    final int IDX_DATE = 1;
    final int IDX_ENTREPRISE = 2;
    final int IDX_PRIX = 3;
    final int IDX_DESCRIPTION = 4;

    String toString(String[][] produits){
        String texteProduit = "";
        for(int i = 0; i < length(produits, 1); i++){
            texteProduit = texteProduit + produits[i][IDX_NOM] + " (";
            texteProduit = texteProduit + produits[i][IDX_DATE] + ") - ";
            texteProduit = texteProduit + produits[i][IDX_PRIX] + " - ";
            texteProduit = texteProduit + produits[i][IDX_DESCRIPTION] + "\n\n";
        }
        return texteProduit;
    }

    String[][] chargerProduits(String repertoire, String prefixe) {
        String[] tabFichier = getAllFilesFromDirectory(repertoire);
        String[][] Produits = new String[length(tabFichier)][5];
        String[] Produit;
        for(int i = 1; i <= length(tabFichier); i = i +1){
            String contenueFichier = fileAsString(repertoire+prefixe+(i)+".txt");
            Produit = getInfo(contenueFichier);
            Produits[i-1] = Produit;
        }
        return Produits;
    }

    String[] getInfo(String contenueFile){
        String[] retour = new String[5];
        int x = 0;
        boolean points = false;
        String chaine = "";
        String temp = "";
        for (int i=1; i<length(contenueFile); i++) {
            if (charAt(contenueFile, i-1) == ':' && charAt(contenueFile, i) == ' ') {
                points = true;
            }else if (points) {
                if (charAt(contenueFile, i) == '\n') {
                    retour[x] = chaine;
                    chaine = "";
                    x = x+1;
                    points = false;
                } else {
                    chaine=chaine+charAt(contenueFile, i);
                }
            }
        }
        return retour;
    }

    String description(String fileName){
        String retour = "";
        if (fileName == "index"){
            retour = "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>\n";
            retour = retour + "          <p>\n";
            retour = retour + "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique.";
            retour = retour + " Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique";
            retour = retour + " que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui";
            retour = retour + " ont contribué au développement du secteur informatique.\n";
            retour = retour + "          </p>\n";
        } else {
            String[] values = getInfo(fileAsString(fileName));
            retour = "        <h2>"+ values[0] + " (" + values[2] + ")</h2>\n";
            retour = retour + "        <h3>"+ values[3] + " (Sortie en " + values[1] + ")</h3>\n";
            retour = retour + "        <p>\n";
            retour = retour + values[4] + "\n";
            retour = retour + "        </p>\n";

        }
        return retour;
    }

    String genererPage(String fileName, int numProduit) {
        String retour = "";
        retour = retour + "<!DOCTYPE html>\n";
        retour = retour + "<html lang=\"fr\">\n";
        retour = retour + "  <head>\n";
        retour = retour + "    <title>Ordinateurs mythiques</title>\n";
        retour = retour + "    <meta charset=\"utf-8\">\n";
        retour = retour + "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n";
        retour = retour + "  </head>\n";
        retour = retour + "  <body>\n";
        retour = retour + "    <header>\n";
        retour = retour + "      <h1>Ordinateurs mythiques</h1>\n";
        retour = retour + "    </header>\n";
        retour = retour + "    <nav>\n";
        retour = retour + "      <ul>\n";
        retour = retour + "        <li><a href=\"index.html\">Accueil</a></li>\n";
        retour = retour + "        <li><a href=\"produit1.html\">Produit 1</a></li>\n";
        retour = retour + "        <li><a href=\"produit2.html\">Produit 2</a></li>\n";
        retour = retour + "        <li><a href=\"produit3.html\">Produit 3</a></li>\n";
        retour = retour + "        <li><a href=\"produit4.html\">Produit 4</a></li>\n";
        retour = retour + "        <li><a href=\"produit5.html\">Produit 5</a></li>\n";
        retour = retour + "      </ul>\n";
        retour = retour + "    </nav>\n";
        retour = retour + "    <main>\n";
        retour = retour + "      <section>\n";
        retour = retour + description(fileName);
        retour = retour + "      </section>\n";
        retour = retour + "    </main>\n";
        retour = retour + "  </body>\n";
        retour = retour + "</html>\n";
        return retour;
    }

    void algorithm() {
        println(toString(chargerProduits("data/", "produit")));
    }
}