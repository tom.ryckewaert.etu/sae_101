//! For some reason, a blank ".html" file is created, no fixes have been found.

class GenerateurProduit extends Program
{
    final String ERROR_MSG = "File isn't valid";
    String[] getSubString(String fileContent)
    {
        // var init
        String[] subString = new String[5];
        int subStringIdx = 0;
        boolean IsPointCharFound = false;
        String finalString = "";

        for (int i=1; i<length(fileContent); i++)
        {
            if (charAt(fileContent, i-1) == ':' && charAt(fileContent, i) == ' ')
            {
                IsPointCharFound = true;
            }
            else if (IsPointCharFound)
            {
                if (charAt(fileContent, i) == '\n')
                {
                    subString[subStringIdx] = finalString;
                    finalString = "";
                    subStringIdx = subStringIdx+1;
                    IsPointCharFound = false;
                }
                else
                {
                    finalString=finalString+charAt(fileContent, i);
                }
            }
        }
        return subString;
    }

    void algorithm()
    {
        String fileName = argument(0);
        if (fileExist(fileName))
        {
            String fileContent = fileAsString(fileName);
            String[] subStringVal = getSubString(fileContent);
            println("<!DOCTYPE html>");
            println("<html lang=\"fr\">");
            println("  <head>");
            println("    <title>"+ subStringVal[0] +"</title>");
            println("    <meta charset=\"utf-8\">");
            println("  </head>");
            println("  <body>");
            println("    <h1>"+ subStringVal[0] + " (" + subStringVal[2] + ")</h1>");
            println("    <h2>"+ subStringVal[3] + " (Sortie en " + subStringVal[1] + ")</h2>");
            println("    <p>");
            println(subStringVal[4]);
            println("    </p>");
            println("  </body>");
            println("</html>");
        }
        else
        {
            error(ERROR_MSG);
        }

    }

}