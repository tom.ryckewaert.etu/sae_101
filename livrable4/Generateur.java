class Generateur extends Program
{

    final char NEW_LINE = '\n';
    final int IDX_NOM = 0;
    final int IDX_DATE = 1;
    final int IDX_ENTREPRISE = 2;
    final int IDX_PRIX = 3;
    final int IDX_DESCRIPTION = 4;

    void testCopy()
    {
        String[][] produits = chargerProduits("data/", "produit");
        String[][] copie = copy(produits);
        assertArrayEquals(produits, copie);
    }

    String[][] copy(String[][] tableau)
    {
        String[][] copie = new String[length(tableau,1)][length(tableau,2)];
        for (int x=0; x<length(copie, 1); x = x+1)
        {
            for (int y=0; y<length(copie,2); y = y+1)
            {
                copie[x][y] = tableau[x][y];
            }
        }
        return copie;
    }

    String toString(String[][] produits)
    {
        String texteProduit = "";
        for (int i = 0; i < length(produits, 1); i = i+1){
            texteProduit = texteProduit + produits[i][IDX_NOM] + " (";
            texteProduit = texteProduit + produits[i][IDX_DATE] + ") - ";
            texteProduit = texteProduit + produits[i][IDX_PRIX] + " - ";
            texteProduit = texteProduit + produits[i][IDX_DESCRIPTION] + "\n\n";
        }
        return texteProduit;
    }

    String rechercherValeur(String chaine, String cle)
    {
        String valeur = "";
        int indice = 0;
        while (indice < length(chaine) && indice+length(cle) < length(chaine) && !equals(cle, substring(chaine, indice, indice+length(cle))))
        {
            indice = indice + 1;
        }
        if (indice < length(chaine)-length(cle))
        {
            int indiceRetourLigne = indice;
            while (indiceRetourLigne < length(chaine) && charAt(chaine, indiceRetourLigne) != NEW_LINE)
            {
                indiceRetourLigne = indiceRetourLigne + 1;
            }
            valeur = substring(chaine, indice+length(cle), indiceRetourLigne);
        }
        return valeur;
    }

    String[][] chargerProduits(String repertoire, String prefixe)
    {
        String[] tabFichier = getAllFilesFromDirectory(repertoire);
        String[][] produits = new String[length(tabFichier)][5];
        for (int i = 0; i<length(produits); i = i+1)
        {
            String contenueFichier = fileAsString(repertoire+prefixe+(i+1)+".txt");
            produits[i][IDX_NOM] = rechercherValeur(contenueFichier, "nom : ");
            produits[i][IDX_DATE] = rechercherValeur(contenueFichier, "date : ");
            produits[i][IDX_PRIX] = rechercherValeur(contenueFichier, "prix : ");
            produits[i][IDX_ENTREPRISE] = rechercherValeur(contenueFichier, "entreprise : ");
            produits[i][IDX_DESCRIPTION] = rechercherValeur(contenueFichier, "description : ");
        }
        return produits;
    }

    String description(int numProduit, String[][] produits, boolean index)
    {
        String retour = "";
        if (index){
            retour = "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>\n";
            retour = retour + "          <p>\n";
            retour = retour + "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique.";
            retour = retour + " Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique";
            retour = retour + " que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui";
            retour = retour + " ont contribué au développement du secteur informatique.\n";
            retour = retour + "          </p>\n";
        }
        else
        {
            retour = "        <h2>"+ produits[numProduit][IDX_NOM] + " (" + produits[numProduit][IDX_ENTREPRISE] + ")</h2>\n";
            retour = retour + "        <h3>"+ produits[numProduit][IDX_PRIX] + " (Sortie en " + produits[numProduit][IDX_DATE] + ")</h3>\n";
            retour = retour + "        <p>\n";
            retour = retour + produits[numProduit][IDX_DESCRIPTION] + "\n";
            retour = retour + "        </p>\n";

        }
        return retour;
    }

    String genererPage(int numProduit, String[][] produits, boolean index)
    {
        String retour = "";
        retour = retour + "<!DOCTYPE html>\n";
        retour = retour + "<html lang=\"fr\">\n";
        retour = retour + "  <head>\n";
        retour = retour + "    <title>Ordinateurs mythiques</title>\n";
        retour = retour + "    <meta charset=\"utf-8\">\n";
        retour = retour + "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n";
        retour = retour + "  </head>\n";
        retour = retour + "  <body>\n";
        retour = retour + "    <header>\n";
        retour = retour + "      <h1>Ordinateurs mythiques</h1>\n";
        retour = retour + "    </header>\n";
        retour = retour + "    <nav>\n";
        retour = retour + "      <ul>\n";
        retour = retour + "        <li><a href=\"index.html\">Accueil</a></li>\n";
        retour = retour + "        <li><a href=\"produit"+ (numProduit+1) +".html\">"+produits[numProduit][IDX_NOM]+"</a></li>\n";
        if (numProduit < length(produits,1) - 1)
        {
            retour = retour + "        <li><a href=\"produit"+(numProduit+2)+".html\">"+produits[numProduit+1][IDX_NOM]+"</a></li>\n";
        }
        if (numProduit < length(produits,1) - 2)
        {
            retour = retour + "        <li><a href=\"produit"+(numProduit+3)+".html\">"+produits[numProduit+2][IDX_NOM]+"</a></li>\n";
        }
        if (numProduit < length(produits,1) - 3)
        {
            retour = retour + "        <li><a href=\"produit"+(numProduit+4)+".html\">"+produits[numProduit+3][IDX_NOM]+"</a></li>\n";
        }
        if (numProduit < length(produits,1) - 4)
        {
            retour = retour + "        <li><a href=\"produit"+(numProduit+5)+".html\">"+produits[numProduit+4][IDX_NOM]+"</a></li>\n";
        }
        retour = retour + "\n<li><a href=\"produits-nom.html\">Produits</a></li>\n";
        retour = retour + "      </ul>\n";
        retour = retour + "    </nav>\n";
        retour = retour + "    <main>\n";
        retour = retour + "      <section>\n";
        retour = retour + description(numProduit, produits, index);
        retour = retour + "      </section>\n";
        retour = retour + "    </main>\n";
        retour = retour + "  </body>\n";
        retour = retour + "</html>\n";
        return retour;
    }

    String genererPageTrie(String[][] produits, int colonne){
        String[][] produitsTriee = copy(produits);
        trierSurColonne(produitsTriee, colonne);
        String retour = "";
        retour = retour + "<!DOCTYPE html>\n";
        retour = retour + "<html lang=\"fr\">\n";
        retour = retour + "  <head>\n";
        retour = retour + "    <title>Ordinateurs mythiques</title>\n";
        retour = retour + "    <meta charset=\"utf-8\">\n";
        retour = retour + "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n";
        retour = retour + "  </head>\n";
        retour = retour + "  <body>\n";
        retour = retour + "    <header>\n";
        retour = retour + "      <h1>Ordinateurs mythiques</h1>\n";
        retour = retour + "    </header>\n";
        retour = retour + "    <nav>\n";
        retour = retour + "      <ul>\n";
        retour = retour + "        <li><a href=\"index.html\">Accueil</a></li>\n";
        retour = retour + "<li><a href=\"produits-nom.html\">Produits</a></li>\n";
        retour = retour + "      </ul>\n";
        retour = retour + "    </nav>\n";
        retour = retour + "    <main>\n";
        retour = retour + "      <section>\n";
        retour = retour + "        <h2>Liste de l'ensemble des ordinateurs</h2>\n";
        retour = retour + "          <p>\n";
        retour = retour + "Trier sur : <a href=\"produits-nom.html\">NOM</a>, <a href=\"produits-date.html\">DATE</a>, <a href=\"produits-prix.html\">PRIX</a>, <a href=\"produits-entreprise.html\">ENTREPRISE</a>, <a href=\"produits-description.html\">DESCRIPTION</a>.\n";
        retour = retour + "            <table>\n";
        for (int x=0; x<length(produitsTriee,1); x = x+1)
        {
            retour = retour + "              <tr>\n";
            retour = retour + "                <td>" + produitsTriee[x][IDX_NOM] + "</td>";
            retour = retour + "<td>" + produitsTriee[x][IDX_DATE] + "</td>";
            retour = retour + "<td>" + produitsTriee[x][IDX_PRIX] + "</td>";
            retour = retour + "<td>" + produitsTriee[x][IDX_ENTREPRISE] + "</td>";
            retour = retour + "<td>" + produitsTriee[x][IDX_DESCRIPTION] + "</td>\n";
            retour = retour + "              </tr>\n";
        }
        retour = retour + "            </table>\n";
        retour = retour + "          </p>\n";
        retour = retour + "      </section>\n";
        retour = retour + "    </main>\n";
        retour = retour + "  </body>\n";
        retour = retour + "</html>\n\n";
        return retour;
    }

    void testPermuterLignes()
    {
        String[][] t = new String[][]{{"a","b","c"}, {"D", "E", "F"}};
        permuterLigne(t,0,1);
        assertArrayEquals(new String[][]{{"D", "E", "F"}, {"a", "b", "c"}}, t);
    }

    void permuterLigne(String[][] tableau, int ligne1, int ligne2){
        String[] temp = tableau[ligne1];
        tableau[ligne1] = tableau[ligne2];
        tableau[ligne2] = temp;
    }


    void testTrierSurColonne()
    {
        String[][] t = new String[][]{{"a1", "b3", "c2"},
                                      {"a1", "b2", "c3"},
                                      {"a1", "b1", "c1"}};
        trierSurColonne(t, 0); // on trie sur la première colonne
        assertArrayEquals(new String[][]{{"a1", "b3", "c2"},
                                         {"a1", "b2", "c1"},
                                         {"a1", "b1", "c3"}}, t);
        trierSurColonne(t, 1); // on trie sur la deuxième colonne
        assertArrayEquals(new String[][]{{"a2", "b1", "c1"},
                                         {"a3", "b2", "c3"},
                                         {"a1", "b3", "c2"}}, t);
        trierSurColonne(t, 2); // on trie sur la troisième colonne
        assertArrayEquals(new String[][]{{"a2", "b1", "c1"},
                                         {"a1", "b3", "c2"},
                                         {"a3", "b2", "c3"}}, t);
    }

    void _trierSurColonne(String[][] tableau, int colonne){
        for (int i = 0; i<length(tableau, 1) - 1; i = i+1)
        {
            for (int j = 0; j<length(tableau, 1)-i-1; j = j+1)
            {
                if (compare(tableau[j][colonne], tableau[j+1][colonne]) >= 0){
                    permuterLigne(tableau, j, j+1);
                }
            }
        }
    }

    void trierSurColonne(String[][] tableau, int colonne){
        int borneMax = length(tableau) - 1;
        boolean permutation = true;
        while (permutation){
            permutation = false;
            for (int i = 0; i<borneMax; i = i+1){
                if (compare(tableau[i][colonne], tableau[i+1][colonne]) > 0)
                {
                    permuterLigne(tableau, i, i+1);
                    permutation = true;
                }
            }
            borneMax = borneMax - 1;
        }
    }

    void _algorithm()
    {
        String[][] produits = chargerProduits("data/", "produit");
        stringAsFile("output/index.html", genererPage(0, produits, true));
        for (int i = 0; i<length(produits, 1); i = i+1)
        {
            stringAsFile("output/produit"+(i+1)+".html", genererPage(i, produits, false));
        }
        stringAsFile("output/produits-nom.html", genererPageTrie(produits, IDX_NOM));
        stringAsFile("output/produits-date.html", genererPageTrie(produits, IDX_DATE));
        stringAsFile("output/produits-prix.html", genererPageTrie(produits, IDX_PRIX));
        stringAsFile("output/produits-description.html", genererPageTrie(produits, IDX_DESCRIPTION));
        stringAsFile("output/produits-entreprise.html", genererPageTrie(produits, IDX_ENTREPRISE));
    }
}
