class GenerateurSite extends Program
{

    String[] getSubString(String fileContent)
    {
        // var init
        String[] subString = new String[5];
        int subStringIdx = 0;
        boolean IsPointCharFound = false;
        String finalString = "";
        for (int firstIdx = 1; firstIdx < length(fileContent); firstIdx = firstIdx + 1)
        {
            if (charAt(fileContent, firstIdx-1) == ':' && charAt(fileContent, firstIdx) == ' ')
            {
                IsPointCharFound = true;
            }
            else if (IsPointCharFound)
            {
                if (charAt(fileContent, firstIdx) == '\n')
                {
                    subString[subStringIdx] = finalString;
                    finalString = "";
                    subStringIdx = subStringIdx+1;
                    IsPointCharFound = false;
                }
                else
                {
                    finalString=finalString+charAt(fileContent, firstIdx);
                }
            }
        }
        return subString;
    }

    String GetFilDesc(String fileName){
        String subString = "";
        if (fileName == "index")
        {
            subString = "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>\n";
            subString = subString + "          <p>\n";
            subString = subString + "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique.";
            subString = subString + " Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique";
            subString = subString + " que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui";
            subString = subString + " ont contribué au développement du secteur informatique.\n";
            subString = subString + "          </p>\n";
        }
        else
        {
            String[] values = getSubString(fileAsString(fileName));
            subString = "        <h2>"+ values[0] + " (" + values[2] + ")</h2>\n";
            subString = subString + "        <h3>"+ values[3] + " (Sortie en " + values[1] + ")</h3>\n";
            subString = subString + "        <p>\n";
            subString = subString + values[4] + "\n";
            subString = subString + "        </p>\n";

        }
        return subString;
    }

    String GenerateWebPage(String fileName) {
        String subString = "";
        subString = subString + "<!DOCTYPE html>\n";
        subString = subString + "<html lang=\"fr\">\n";
        subString = subString + "  <head>\n";
        subString = subString + "    <title>Ordinateurs mythiques</title>\n";
        subString = subString + "    <meta charset=\"utf-8\">\n";
        subString = subString + "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n";
        subString = subString + "  </head>\n";
        subString = subString + "  <body>\n";
        subString = subString + "    <header>\n";
        subString = subString + "      <h1>Ordinateurs mythiques</h1>\n";
        subString = subString + "    </header>\n";
        subString = subString + "    <nav>\n";
        subString = subString + "      <ul>\n";
        subString = subString + "        <li><a href=\"index.html\">Accueil</a></li>\n";
        subString = subString + "        <li><a href=\"produit1.html\">Produit 1</a></li>\n";
        subString = subString + "        <li><a href=\"produit2.html\">Produit 2</a></li>\n";
        subString = subString + "        <li><a href=\"produit3.html\">Produit 3</a></li>\n";
        subString = subString + "        <li><a href=\"produit4.html\">Produit 4</a></li>\n";
        subString = subString + "        <li><a href=\"produit5.html\">Produit 5</a></li>\n";
        subString = subString + "      </ul>\n";
        subString = subString + "    </nav>\n";
        subString = subString + "    <main>\n";
        subString = subString + "      <section>\n";
        subString = subString + GetFilDesc(fileName);
        subString = subString + "      </section>\n";
        subString = subString + "    </main>\n";
        subString = subString + "  </body>\n";
        subString = subString + "</html>\n";
        return subString;
    }

    void algorithm() {
        stringAsFile("output/index.html", GenerateWebPage("index"));
        for (int subStringIdx = 1; subStringIdx<=5; subStringIdx= subStringIdx + 1)
        {
            stringAsFile("output/produit" + subStringIdx + ".html", GenerateWebPage("data/produit" + subStringIdx + ".txt"));
        }
    }

} 